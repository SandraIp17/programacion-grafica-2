from OpenGL.GL import *
from OpenGL.GLU import *

class Esfera:
    # Inicializador
    def __init__(self, gl):
        self.gl = gl

    #Funcion para Crear una esfera
    def crearEsfera(self,tx,ty,tz,rx,ry,rz,r,g,b,rad,cor,pila):

        self.gl.glPushMatrix()
        self.gl.glTranslatef(tx,ty,tz)
        self.gl.glRotatef(rx,1,0,0)
        self.gl.glRotatef(ry, 0,1,0)
        self.gl.glRotatef(rz, 0,0,1)
        self.gl.glColor3f(r,g,b)

        #gluSphere (POINTER (GLUquadric) (quad), GLdouble (radio), GLint (cortes), GLint (pilas))
        gluSphere(gluNewQuadric(), rad,cor,pila)
        self.gl.glPopMatrix()

