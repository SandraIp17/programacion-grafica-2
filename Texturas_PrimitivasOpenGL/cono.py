# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Diciembre 2020
# Clase Cubo
# Descripción: Una clase que mantiene todos los componentes necesarios para la creación del cubo y sus diferentes transformaciones
# Es llamada por la clase main.

import math
import numpy as np
import OpenGL.GLUT.special as glt
from OpenGL.GLUT import *

from Texturas import *

class Cono:
    #Inicializador
    def __init__(self,gl):
        self.gl = gl
        self.t = Texturas("./imagenes/grecia.jpg","./imagenes/sushitex.png")

    #Función para crear un cubo
    def crearCono(self,ratio,h,ex,ey,ez,tx,ty,tz,rx,ry,rz):

        self.gl.glPushMatrix()
        #Transformaciones
        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        #self.gl.glColor3f(1, 0, 1)
        self.gl.glBindTexture(GL_TEXTURE_2D, self.t.texture_id)

        #Se define la base
        self.gl.glBegin(self.gl.GL_POLYGON)
        for i in np.arange(0.0, 10.0, 0.1):
            x = ratio * math.cos(i)
            y = ratio * math.sin(i)
            glTexCoord2f(x, y)
            glVertex3f(x, y, 0)
        self.gl.glEnd()

        #self.tx1.crear()
        #Se define triángulos alrededor de la base que convergen a un punto
        self.gl.glBegin(self.gl.GL_TRIANGLE_FAN)
        #self.gl.glColor3f(1, 0, 0)

        glTexCoord2f(0, 0)
        self.gl.glVertex3f(0, 0, h)
        for i in np.arange(0.0, 10.0, 0.1):
            x = ratio * math.cos(i)
            y = ratio * math.sin(i)
            glTexCoord2f(x, y)
            self.gl.glVertex3f(x, y, 0)
        self.gl.glEnd()

        self.gl.glPopMatrix()

    def destructor(self):
        self.tx1.destructor()