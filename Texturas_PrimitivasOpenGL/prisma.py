# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Diciembree 2020
# Clase prisma
# Descripción: Una clase que mantiene todos los componentes necesarios para la creación del prisma y sus diferentes transformaciones
# Es llamada por la clase main.
# Incluye texturas

import math
from Texturas import *
class prisma:
    #Inicializador
    def __init__(self,gl):
        self.gl = gl
        self.t= Texturas("./imagenes/stone2.jpg","./imagenes/Hojas.jpg")

    def crearPrisma(self, x,y,z,w,h,p,rx,ry,rz):
        self.gl.glPushMatrix()
        self.gl.glTranslatef(x, y, z)
        self.gl.glRotatef(rx, 1, 0, 0)
        self.gl.glRotatef(ry, 0, 1, 0)
        self.gl.glRotatef(rz, 0, 0, 1)
        self.gl.glScalef(w, h, p)

        #cargar textura
        self.gl.glBindTexture(GL_TEXTURE_2D, self.t.texture_id1)

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(1, 0, 0)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        self.gl.glTexCoord3f(0, 1, 0)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        self.gl.glTexCoord3f(0, 0, 1)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1, 1)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        self.gl.glTexCoord3f(1, 1, 1)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        self.gl.glTexCoord3f(0, -1, 1)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1, 1)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        self.gl.glTexCoord3f(1, 1, 1)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        self.gl.glTexCoord3f(0, -1, 1)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1, 1)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        self.gl.glTexCoord3f(1, 1, 1)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        self.gl.glTexCoord3f(0, -1, 1)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        self.gl.glEnd()
        self.gl.glBindTexture(GL_TEXTURE_2D, self.t.texture_id)
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1, 1)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        self.gl.glTexCoord3f(1, 1, 1)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        self.gl.glTexCoord3f(0, -1, 1)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1, 1)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        self.gl.glTexCoord3f(1, 1, 1)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        self.gl.glTexCoord3f(0, -1, 1)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1, 1)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        self.gl.glTexCoord3f(1, 1, 1)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        self.gl.glTexCoord3f(0, -1, 1)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1, 1)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        self.gl.glTexCoord3f(1, 1, 1)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        self.gl.glTexCoord3f(0, -1, 1)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        self.gl.glEnd()

        self.gl.glPopMatrix()