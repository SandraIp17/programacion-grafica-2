# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Diciembre 2020
# Clase Cilindro
# Descripción: Una clase que mantiene todos los componentes necesarios para la creación del cilindro con texturas
# Es llamada por la clase Main.

from OpenGL.GLU import *
from Texturas import *

class cilindro:
    #Inicializador
   def __init__(self, gl):
        self.gl = gl
        self.t = Texturas("./imagenes/Hojas.jpg","./imagenes/Hojas.jpg")
        self.cylinder = gluNewQuadric()

    #Creación del cilindro
   def crearCilindro(self,base,top,h, sl, st, ex,ey,ez,tx,ty,tz,rx,ry,rz):
       self.gl.glPushMatrix()
        #Transformaciones
       self.gl.glScalef(ex, ey, ez)
       self.gl.glTranslate(tx, ty, tz)
       self.gl.glRotate(rx, 1, 0, 0)
       self.gl.glRotate(ry, 0, 1, 0)
       self.gl.glRotate(rz, 0, 0, 1)

        #Dibujamos el cilindro
       gluQuadricDrawStyle(self.cylinder, GLU_FILL);
       gluQuadricTexture(self.cylinder, True);
       gluQuadricNormals(self.cylinder, GLU_SMOOTH);
       self.gl.glBindTexture(GL_TEXTURE_2D, self.t.texture_id)
       gluCylinder(self.cylinder,base,top,h, sl, st)

       self.gl.glPopMatrix()
